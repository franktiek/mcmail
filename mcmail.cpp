/*	mcmail.cpp : Defines the entry point for the console application.

	Copyright (c) 2005 by Industrial Development & Engineering
	ALL RIGHTS RESERVED.

	Revision History:
	-----------------

*/


#include "..\main\portable.hpp"

#include <windows.h>
#include <wincrypt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#ifndef UNIX
#include <share.h>
#include <io.h>
#include <conio.h>
#endif

#include <chilkat/CkImap.h>
#include <chilkat/CkMessageSet.h>
#include <chilkat/CkMailBoxes.h>
#include <chilkat/CkMailMan.h>
#include <chilkat/CkEmail.h>
#include <chilkat/CkEmailBundle.h>
#include <chilkat/CkString.h>
#include <chilkat/CkSettings.h>

#include "..\main\version.hpp"

#define MAIL_OKAY	0

#define	MAIL_USAGE_NONE		0
#define	MAIL_USAGE_SMTP		1
#define	MAIL_USAGE_POP3		2
#define	MAIL_USAGE_IMAP		3

extern		int		mcMailPOP3(char *pop3ServerStr, char *userStr, char *passwordStr, int portNo = -1);
extern		int		mcMailIMAP(char *imapServerStr, char *userStr, char *passwordStr, char *mailboxStr, int portNo = -1);
extern		int		mcMailSMTP(char *smtpServerStr, char *userStr, char *passwordStr, char *fromStr, char *toStr, char *bodyStr, char *subjectStr, char *attachStr, int portNo=-1);

			bool	sslFlag=false;

int main(int argc, char* argv[])
/*

*/
{
	bool		usageFlag		= false;
	int			portNo			= -1;
	int         rc				= 0;
	int         usageType		= 0;
	char*		s				= NULL;
	char*		exePathPtr		= NULL;
	char		usrStr[255]		= "";
	char		passwdStr[255]	= "";
	char		exeName[9];
	char		upperExeName[9];
	struct tm   *newTime;
	time_t      aClock;

	//-------------------- get exec name 
	exePathPtr	= strdup(argv[0]);

	// Strip extension
	if (strrchr(exePathPtr, '.') > strrchr(exePathPtr, '\\'))
		*(strrchr(exePathPtr, '.'))	= '\0';

	// Find the last path-separator
	if (strrchr(exePathPtr, '\\'))
		sprintf_s(exeName, "%-8.8s", strrchr(exePathPtr, '\\') + 1);
	else
		sprintf_s(exeName, "%-8.8s", exePathPtr);

	free(exePathPtr);

	strcpy_s(upperExeName, exeName);

	for (size_t i = 0; i < strlen(upperExeName); i++) {
		upperExeName[i]	= toupper(upperExeName[i]);
	}

	time( &aClock );
	newTime = localtime(&aClock);

	printf("\n %s     McMain Mail Utility: %s", upperExeName, asctime(newTime));
#ifdef CBCLT
	printf(" Version %s%s  dBase IV                 Copyright I.D.E. (c) 1991 - 2003\n\n", MC_VERSION_ID, MCUTL_REV_ID);
#endif

	if ( argc < 2 || (argv[1][0]!='-' && argv[1][0]!='/') ) {
		usageFlag	= true;
	}
	else {
		switch (argv[1][1]) {
		case 'a':
		case 'A':
			if (argc<8)
				usageFlag	= true;
			else {
				usageType	= MAIL_USAGE_SMTP;

				if (argc>8) {
					strcpy_s(usrStr, argv[8]);
					if ((s = strchr(usrStr, ':')) != NULL) {
						strcpy_s(passwdStr, s+1);
						*s	= '\0';
					}
				}

				rc = mcMailSMTP(argv[2], usrStr, passwdStr, argv[3], argv[4], argv[5], argv[6], argv[7], portNo);
			}
			break;

		case 'P':
			sslFlag	= true;
			portNo	= 995;
		case 'p':
			usageType	= MAIL_USAGE_POP3;

			if (argv[1][2])
				portNo = atoi(argv[1] + 2);

			if (argc>2) {
				strcpy_s(usrStr, argv[3]);
				if ((s = strchr(usrStr, ':')) != NULL) {
					strcpy_s(passwdStr, s+1);
					*s	= '\0';
				}
			}

			rc = mcMailPOP3(argv[2], usrStr, passwdStr, portNo);
			break;

		case 'I':
			sslFlag	= true;
			portNo	= 993;
		case 'i':
			usageType	= MAIL_USAGE_IMAP;

			if (argv[1][2])
				portNo = atoi(argv[1] + 2);

			if (argc>2) {
				strcpy_s(usrStr,	argv[3]);
				if ((s = strchr(usrStr, ':')) != NULL) {
					strcpy_s(passwdStr, s+1);
					*s	= '\0';
				}
			}

			rc = mcMailIMAP(argv[2], usrStr, passwdStr, argv[4], portNo);
			break;

		case 'S':
			sslFlag	= true;
			portNo	= 587;
		case 's':
			if (argc<7)
				usageFlag	= true;
			else {
				usageType	= MAIL_USAGE_SMTP;

				if (argc>7) {
					strcpy_s(usrStr,	argv[7]);
					if ( (s = strchr(usrStr, ':')) != NULL) {
						strcpy_s(passwdStr, s+1);
						*s	= '\0';
					}
				}

				rc = mcMailSMTP(argv[2], usrStr, passwdStr, argv[3], argv[4], argv[5], argv[6], NULL, portNo);
			}
			break;

		default:
			usageFlag	= true;
		}
	}

	if (usageFlag) {
		printf("Usage:\r\nSend smtp e-mail+attachment: %s -a smtpserver from to subject body attach [user:password]\r\n", exeName);
		printf("Send smtp e-mail message: %s -s smtpserver from to subject body [user:password]\r\n", exeName);
		printf("Get message count POP3 server: %s -p[portno] pop3server [user:password]\r\n", exeName);
		printf("Get mailbox list (all) IMAP server: %s -i[portno] imapserver [user:password] *\r\n", exeName);
		printf("Get folder list (auth) IMAP server: %s -i[portno] imapserver [user:password]\r\n", exeName);
		printf("Get message count IMAP mailbox: %s -i imapserver [user:password] mailbox\r\n", exeName);
		printf("SSL counterparts of SMTP use -S (port 587)\r\n", exeName);
		printf("SSL counterparts of IMAP use -I (port 993), POP3 uses -P (port 995)\r\n", exeName);
		return(-1);
	}

	return (rc);
}




int		mcMailIMAP(char *imapServerStr, char *userStr, char *passwordStr, char *mailboxStr, int portNo)
/*

	Obtain message count from specified IMAP server

	//  Load a certificate from a PFX file and use it.
	//  Note: Other methods are available to load pre-installed
	//  certificates from registry-based certificate stores.

	//  Create an instance of a certificate store object, load a PFX file,
	//  locate the certificate we need, and use it for signing.
	//  (a PFX file may contain more than one certificate.)
	CkCertStore certStore;
	//  The 1st argument is the filename, the 2nd arg is the
	//  PFX file's password:
	success = certStore.LoadPfxFile("myCertWithPrivateKey.pfx","secret");
	if (success != true) {
		printf("%s\n",certStore.lastErrorText());
		return;
	}

	//  Find the certificate by the subject common name:
	CkCert *cert = 0;
	cert = certStore.FindCertBySubjectCN("Chilkat Software, Inc.");
	if (cert == 0 ) {
		printf("%s\n",certStore.lastErrorText());
		return;
	}

	//  If a PFX file is known to contain a single certificate,
	//  you may load it directly into a Chilkat certificate object.
	//  This snippet of source code shows how:
	CkCert cert2;
	//  The 1st argument is the filename, the 2nd arg is the
	//  PFX file's password:
	success = cert2.LoadPfxFile("tagtooga_secret.pfx","secret");
	if (success != true) {
		printf("%s\n",cert->lastErrorText());
		return;
	}

	//  Use the cert:
	imap.SetSslClientCert(*cert);

*/
{
	int			rc=0, i, nFld;
	char		msgStr[255];
	CkMailMan	mailman;
	CkImap		imapman;
	CkString	strError, refName, wildcardedMailbox;
	CkMailboxes *mboxes=0;


	// unlock chilkat components
	imapman.UnlockComponent("SIDESoftIMAPMAIL_UGRy1lnl5HmH");

	if (portNo > 0)
		imapman.put_Port(portNo);

	if (sslFlag)
		imapman.put_Ssl(true);

	if (userStr && userStr[0]) {
		rc	= imapman.Connect(imapServerStr);

		if (!imapman.IsConnected()) {
			sprintf_s(msgStr, "Error connecting to imap server %s (%s)", imapServerStr, userStr);
			printf(msgStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}
		else
			printf("Verify connection OK\n");

		rc	= imapman.Login(userStr, passwordStr);

		if (rc==false) {
			printf("Error logging on imap server %s (%s)\n", imapServerStr, userStr);
			printf("%s\n", imapman.lastErrorText());
			return(-3);
		}
		else
			printf("Verify login OK\n");
#ifdef NEP
		if (!imapman.IsLoggedIn()) {
			sprintf(msgStr, "Error logging on imap server %s (%s)", imapServerStr, userStr);
			printf(msgStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}
		else
			printf("Verify login OK\n");
#endif
	}
	else {
		printf("Missing Login Parameters\n");
		return(-2);
	}

	if (mailboxStr && mailboxStr[0]!='\0' && mailboxStr[0]!='*') {
		rc	= imapman.SelectMailbox(mailboxStr);

		if (rc==false) {
			imapman.LastErrorText(strError);
			printf(strError.getString());
			return(-5);
		}

		rc	= imapman.get_NumMessages();

		if (rc==-1) {
			imapman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}
		else {
			sprintf_s(msgStr, "IMAP Server: %ld messages waiting in queue", rc);
			printf(msgStr);
			return(MAIL_OKAY);
		}
	}
	else {
		refName = "";
		// refName is usually set to an empty string.
		// A non-empty reference name argument is the name of a mailbox or a level of
		// mailbox hierarchy, and indicates the context in which the mailbox
		// name is interpreted.

		// Select all mailboxes matching this pattern:
		wildcardedMailbox = "*";

		mboxes = imapman.ListMailboxes(refName,wildcardedMailbox);

		if (mboxes == 0 ) {
			printf("%s\n",imapman.lastErrorText());
			return (-6);
		}

		nFld	= 0;

		for (i = 0; i <= mboxes->get_Count() - 1; i++) {
			if (mailboxStr==NULL || mailboxStr[0]=='\0') {	// show num messages of authorized folders
				rc	= imapman.SelectMailbox(mboxes->getName(i));

				if (rc==false) {
				}
				else {
					rc	= imapman.get_NumMessages();

					if (rc==-1) {
					}
					else {
						sprintf_s(msgStr, "IMAP Folder %s: %ld messages waiting in queue\r\n", mboxes->getName(i), rc);
						printf(msgStr);
						nFld++;
					}
				}
			}
			else {
				printf("%s\r\n",mboxes->getName(i));
				nFld	= -1;
			}
		}

		switch (nFld) {
		case -1:	// print only
			sprintf_s(msgStr, "IMAP Server: %d mailboxes found\r\n", mboxes->get_Count());
			printf(msgStr);
			break;

		case 0:		// no authorized folders
			sprintf_s(msgStr, "IMAP Server: No permissions on folder list\r\n");
			printf(msgStr);
			break;

		default:
			sprintf_s(msgStr, "IMAP Server: %d folders found with permissions\r\n", nFld);
			printf(msgStr);
			break;
		}

		return(MAIL_OKAY);
		// Sample output looks like this:
		// INBOX.vendors.shareit
		// INBOX.oldSupport
		// INBOX.vendors.paypal
		// INBOX.sales
		// INBOX.lists
		// INBOX.Drafts
		// INBOX.vendors.dell
		// INBOX.Trash
		// INBOX.invoiceRequests
		// INBOX.purchases
		// INBOX.vendors.inMotion
		// INBOX.oldEmail
		// INBOX.vendors
		// INBOX.lists.python
		// INBOX.vendors.myhosting
		// INBOX.Templates
		// INBOX.friends
		// INBOX.bounceSamples
		// INBOX.lists.ruby
		// INBOX.vendors.peer1
		// INBOX.Sent
		// INBOX.Junk
		// INBOX
		delete mboxes;
	}

	// Disconnect from the IMAP server.
	imapman.Disconnect();


	return(MAIL_OKAY);
}


		
int		mcMailSMTP(char *smtpServerStr, char *userStr, char *passwordStr, char *fromStr, char *toStr, char *subjectStr, char *bodyStr, char *attachStr, int portNo)
/*

	Send an e-mail message using the specified SMTP server


*/
{
	bool		success = false;
	CkString	strError;
	CkMailMan	mailman;



	// unlock chilkat components
	mailman.UnlockComponent("SIDESoftMAILQ_d1HNwMDl5xTa");

	if (userStr && userStr[0]) {

		mailman.put_SmtpHost(smtpServerStr);

		if (portNo > 0) {
			mailman.put_SmtpPort(portNo);
		}

		if (sslFlag) {
			mailman.put_StartTLS(true);
		}

		mailman.put_SmtpUsername(userStr);
		mailman.put_SmtpPassword(passwordStr);

		if (!mailman.VerifySmtpConnection()) {
			printf("Error establishing connection to smtp server %s", smtpServerStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}					
		else
			printf("Verify connection OK\n");

		if (!mailman.VerifySmtpLogin()) {
			printf("Error logging on smtp server %s (%s)", smtpServerStr, userStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());

		}
		else
			printf("Verify login OK\n");
	}

	if (attachStr==NULL || attachStr[0]=='\0') {
		//success	= mailman.QuickSend(fromStr, toStr, subjectStr, bodyStr, smtpServerStr);

		CkEmail		email;

		mailman.put_SmtpHost(smtpServerStr);

		email.AddTo("", toStr);
		email.put_Subject(subjectStr);
		email.put_From(fromStr);
		email.put_Body(bodyStr);
		success	= mailman.SendEmail(email);
	}
	else {
		CkEmail		*email=NULL;
		CkString	mimeTypeCkStr;

		mailman.put_SmtpHost(smtpServerStr);
		email	= new CkEmail();

		email->AddTo("", toStr);
		email->put_Subject(subjectStr);
		email->put_From(fromStr);
		email->put_Body(bodyStr);
		email->AddFileAttachment(attachStr, mimeTypeCkStr);
		success	= mailman.SendEmail(*email);
	}

	if (success)
		return(MAIL_OKAY);

	//mailman.GetLogText(strError);
	mailman.LastErrorText(strError);

	printf(strError.getString());

	return(-1);
}



int		mcMailPOP3(char *pop3ServerStr, char *userStr, char *passwordStr, int portNo)
/*

	Obtain message count from specified POP3 server

	//  Use our certificate, which is already installed
	//  in our current-user certificate store on the computer.
	CkCert clientCert;
	success = clientCert.LoadByCommonName("Chilkat Software, Inc.");
	if (success != true) {
		printf("%s\n",clientCert.lastErrorText());
		return;
	}

	//  Note: The GMail POP3 server does not require that you
	//  have a client cert.  This example only demonstrates
	//  how you may use a client certificate.  Typically,
	//  higher-security systems may require a client-side SSL cert.
	mailman.SetSslClientCert(clientCert);

*/
{
	long		rc=0;
	char		msgStr[255];
	CkString	strError;
	CkMailMan	mailman;



	// unlock chilkat components
	mailman.UnlockComponent("SIDESoftMAILQ_d1HNwMDl5xTa");

	mailman.put_MailHost(pop3ServerStr);

	if (portNo > 0)
		mailman.put_MailPort(portNo);

	if (sslFlag)
		mailman.put_PopSsl(true);

	if (userStr && userStr[0]) {
		mailman.put_PopUsername(userStr);
		mailman.put_PopPassword(passwordStr);

		if (!mailman.VerifyPopConnection()) {
			sprintf_s(msgStr, "Error connecting to pop3 server %s (%s)", pop3ServerStr, userStr);
			printf(msgStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}
		else
			printf("Verify connection OK\n");
#ifdef NEP
		if (!mailman.VerifyPopLogin()) {
			sprintf(msgStr, "Error logging on pop3 server %s (%s)", pop3ServerStr, userStr);
			printf(msgStr);
			mailman.LastErrorText(strError);
			printf(strError.getString());
			return(-3);
		}
		else
			printf("Verify login OK\n");
#endif
	}
	else {
		printf("Missing Login Parameters\n");
		return(-2);
	}

	rc	= mailman.CheckMail();

	if (rc==-1) {
		mailman.LastErrorText(strError);
		printf(strError.getString());
	}
	else {
		sprintf_s(msgStr, "POP3 Server: %ld messages waiting in queue", rc);
		printf(msgStr);
		return(MAIL_OKAY);
	}

	return(MAIL_OKAY);
}

